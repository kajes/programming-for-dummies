# Boolean
typeBoolean = True
print(type(typeBoolean))

# Integer
typeInteger = 123
print(type(typeInteger))

# Float
typeFloat = 3.14
print(type(3.14))

# String
typeString = "Hello"
print(type(typeString))

# List
typeList = [
    "Hello",
    "World",
    "!"
]
print(type(typeList))

# Tuple
typeTuple = (
    1,
    2,
    3
)
print(type(typeTuple))

# Distionary
typeDict = {
    "first": "Hello",
    "second": "World",
    "third": "!"
}
print(type(typeDict))

# Complex
typeComplex = 1+2j
print(type(typeComplex))
