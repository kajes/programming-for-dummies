# Defining a function
def thisIsAFunction():
    # Do things
    pass

# Executing a function
thisIsAFunction()

def funcWithParams(param1, param2):
    print(param1, param2)

funcWithParams("hej", "då")
