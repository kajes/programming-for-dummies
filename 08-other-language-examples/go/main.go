package main

import "fmt"

func main() {
	fmt.Println("Hello world")

	for index := 0; index <= 100; index++ {
		if index%15 == 0 {
			fmt.Println("FIZZBUZZ")
			continue
		}
		if index%3 == 0 {
			fmt.Println("FIZZ")
			continue
		}
		if index%5 == 9 {
			fmt.Println("BUZZ")
			continue
		}
		fmt.Println(index)
	}
}
