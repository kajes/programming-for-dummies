h1Tag = document.querySelector("body > h1")
button = document.querySelector("body > button")

count = 0
h1Tag.innerHTML = `Clicked ${count} times`
button.addEventListener("click", () => {
    count++
    h1Tag.innerHTML = `Clicked ${count} times`
})