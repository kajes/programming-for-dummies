<?php

echo "Hello World";

for ($i=0; $i <= 100; $i++) { 
    if ($i % 15 == 0) {
        echo "FIZZBUZZ\n";
        continue;
    }
    if ($i % 3 == 0) {
        echo "FIZZ\n";
        continue;
    }
    if ($i % 5 == 0) {
        echo "BUZZ\n";
        continue;
    }
    echo "$i\n";
}